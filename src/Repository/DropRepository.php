<?php

namespace App\Repository;

use App\Entity\Ship;
use App\Entity\User;
use App\Enum\TypeEnum;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping;

/**
 * Class ShipRepository
 *
 * @package App\Repository
 */
class DropRepository extends EntityRepository
{
    public function __construct(EntityManager $em, Mapping\ClassMetadata $class)
    {
        parent::__construct($em, $class);
    }

    /**
     * @param string $string
     * @return array
     */
    public function findLike(string $string, Ship $ship) : array
    {
        $query = $this
            ->createQueryBuilder('d')
            ->where('LOWER(d.description) LIKE LOWER(:search)')
            ->orderBy('d.description', 'ASC')
            ->setParameter('search', '%'.$string.'%')
        ;

        if (count($ship->getDrops()) > 0) {
            $query
                ->andWhere('d NOT IN (:drops)')
                ->setParameter('drops', $ship->getDrops())
            ;
        }


        return $query->getQuery()->getResult();
    }

    public function findWithMissingShips(User $user)
    {
        return $this
            ->createQueryBuilder('d')
            ->addSelect('s')
            ->leftJoin('d.ships', 's')
            ->leftJoin('s.users', 'u', 'WITH', 'u = :user')
            ->where('u IS NULL')
            ->andWhere('s.enabled = :enabled')
            ->andWhere('s.type = :type')
            ->setParameter('user', $user)
            ->setParameter('enabled', true)
            ->setParameter('type', TypeEnum::TYPE_STANDARD)
            ->getQuery()
            ->getResult()
        ;
    }
}
