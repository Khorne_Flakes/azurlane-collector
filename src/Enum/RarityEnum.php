<?php

namespace App\Enum;

/**
 * Class RarityEnum
 *
 * @package App\Enum
 */
class RarityEnum extends Enum
{
    const RARITY_NORMAL = 'normal';
    const RARITY_RARE = 'rare';
    const RARITY_ELITE = 'elite';
    const RARITY_SUPER_RARE = 'super-rare';
}
