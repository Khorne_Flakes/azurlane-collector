<?php

namespace App\Controller\Admin;

use App\Entity\Drop;
use App\Entity\Ship;
use App\Form\Type\Entity\DropType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DropController
 *
 * @package App\Controller
 *
 * @Route("/admin/drops")
 * @IsGranted("ROLE_ADMIN")
 */
class DropController extends AbstractController
{
    /**
     * @Route("", name="app_admin_drop_index", methods={"GET"})
     *
     * @return Response
     */
    public function index() : Response
    {
        $em = $this->getDoctrine()->getManager();

        return $this->render('App/Admin/Drop/index.html.twig', [
            'drops' => $em->getRepository(Drop::class)->findBy([], ['createdAt' => 'ASC'])
        ]);
    }

    /**
     * @Route("/create", name="app_admin_drop_create", methods={"GET", "POST"})
     *
     * @return Response
     */
    public function create(Request $request) : Response
    {
        $drop = new Drop();
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(DropType::class, $drop);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($drop);
            $em->flush();

            return $this->redirect($this->generateUrl('app_admin_drop_index'));
        }

        return $this->render('App/Admin/Drop/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}", name="app_admin_drop_show", methods={"GET", "POST"})
     *
     * @return Response
     */
    public function show(Drop $drop) : Response
    {
       return $this->render('App/Admin/Drop/show.html.twig', [
            'drop' => $drop
        ]);
    }
}
