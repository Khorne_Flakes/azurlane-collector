<?php

namespace App\Enum;

/**
 * Class TypeEnum
 *
 * @package App\Enum
 */
class TypeEnum extends Enum
{
    const TYPE_STANDARD = 'standard';
    const TYPE_RETROFIT = 'retrofit';
    const TYPE_COLLAB = 'collab';
    const TYPE_PR = 'pr';
}
