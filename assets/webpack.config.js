var Encore = require('@symfony/webpack-encore');
var CopyWebpackPlugin = require('copy-webpack-plugin');

Encore
    .setOutputPath('../public/build/')
    .setPublicPath('/build')

    .addEntry('js/app', './js/app.js')
    .addStyleEntry('css/app', './css/app.css')

    .addPlugin(new CopyWebpackPlugin([
        { from: './img', to: 'img' }
    ]))

    .configureUrlLoader({
        images: { limit: 4096 },
        fonts: { limit: 10240 }
    })

    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
    .autoProvidejQuery()
;

module.exports = Encore.getWebpackConfig();
