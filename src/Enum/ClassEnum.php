<?php

namespace App\Enum;

/**
 * Class ClassEnum
 *
 * @package App\Enum
 */
class ClassEnum extends Enum
{
    const CLASS_DESTROYER = 'destroyer';
    const CLASS_LIGHT_CRUISER = 'light-cruiser';
    const CLASS_HEAVY_CRUISER = 'heavy-cruiser';
    const CLASS_BATTLESHIP = 'battleship';
    const CLASS_AIRCRAFT_CARRIER = 'aircraft-carrier';
    const CLASS_REPAIR_SHIP = 'repair-ship';
    const CLASS_SUBMARINE = 'submarine';
}
