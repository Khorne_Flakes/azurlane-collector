<?php

namespace App\Enum;

use Symfony\Component\Translation\TranslatorInterface;

abstract class Enum
{
    /**
     * @return array
     * @throws \ReflectionException
     */
    public static function getAll() : array
    {
        return (new \ReflectionClass(static::class))->getConstants();
    }

    /**
     * @param TranslatorInterface $translator
     * @return array
     * @throws \ReflectionException
     */
    public static function getLabels(TranslatorInterface $translator) : array
    {
        $labels = [];
        $constants = (new \ReflectionClass(static::class))->getConstants();

        foreach ($constants as $constant) {
            $labels[$constant] = $translator->trans(self::guessTransPrefix(). $constant);
        }

        return $labels;
    }

    /**
     * @return string
     */
    private static function guessTransPrefix() : string
    {
        $key = substr(static::class, 4, -4);
        $key = str_replace('\\', '.', $key);
        $key = strtolower($key);

        return "$key.";
    }
}
