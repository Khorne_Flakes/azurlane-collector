<?php

namespace App\Controller;

use App\Entity\Drop;
use App\Entity\Ship;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ShipController
 *
 * @package App\Controller
 */
class ShipController extends AbstractController
{
    /**
     * @Route("/ships", name="app_ship_index", methods={"GET"})
     *
     * @return Response
     */
    public function index() : Response
    {
        $shipTypes = [];

        $results = $this->getDoctrine()->getRepository(Ship::class)->findAllWithUserPossession($this->getUser());

        foreach ($results as $key => $result) {
              if (!isset($shipTypes[$result->getType()])) {
                $shipTypes[$result->getType()] = [];
            }
            $shipTypes[$result->getType()][] = $result;
        }

        return $this->render('App/Home/index.html.twig', [
            'shipTypes' => $shipTypes
        ]);
    }

    /**
     * @Route("/ships/{id}/own", name="app_ship_own", methods={"GET"})
     *
     * @return Response
     */
    public function own($id) : Response
    {
        $user = $this->getUser();
        $ship = $this->getDoctrine()->getRepository(Ship::class)->findOneWithUserPossession($user, $id);

        if ($ship->owned === true) {
            $user->removeShip($ship);
        } else {
            $user->addShip($ship);
        }

        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse(!$ship->owned);
    }

    /**
     * @Route("/ships/advicePerLocation", name="app_ship_advice_per_location", methods={"GET"})
     *
     * @return Response
     */
    public function advicePerLocation() : Response
    {
        $user = $this->getUser();
        $drops = $this->getDoctrine()->getRepository(Drop::class)->findWithMissingShips($user);

        usort($drops, function($a, $b) {
            $countA = \count($a->getShips());
            $countB = \count($b->getShips());
            if ($countA === $countB) {
                return $a->getDescription() <=> $b->getDescription();
            }
            return $countB <=> $countA;
        });

        return $this->render('App/Home/advice-per-location.html.twig', [
            'drops' => $drops
        ]);
    }

    /**
     * @Route("/ships/advicePerShip", name="app_ship_advice_per_ship", methods={"GET"})
     *
     * @return Response
     */
    public function advicePerShip() : Response
    {
        $user = $this->getUser();
        $ships = $this->getDoctrine()->getRepository(Ship::class)->findOwned($user);

        return $this->render('App/Home/advice-per-ship.html.twig', [
            'ships' => $ships
        ]);
    }
}
