<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\Type\Security\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Class SecurityController
 *
 * @package App\Controller
 *
 * @Route("")
 */
class SecurityController extends AbstractController
{
    /**
     * @Route("", name="app_security_sign_in", methods={"GET", "POST"})
     *
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function signIn(AuthenticationUtils $authenticationUtils) : Response
    {
        if ($this->getUser() instanceof User) {
            return $this->redirectToRoute('app_homepage');
        }

        return $this->render('App/Security/sign-in.html.twig', [
            'lastUsername' => $authenticationUtils->getLastUsername(),
            'error' => $authenticationUtils->getLastAuthenticationError(),
        ]);
    }

    /**
     * @Route("/sign-up", name="app_security_sign_up", methods={"GET", "POST"})
     *
     * @return Response
     */
    public function signUp(Request $request) : Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirect($this->generateUrl('app_homepage'));
        }

        return $this->render('App/Security/sign-up.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
