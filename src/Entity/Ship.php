<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Ship
 *
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\ShipRepository")
 * @ORM\Table(name="alc_ship")
 */
class Ship
{
    /**
     * @var \Ramsey\Uuid\UuidInterface
     *
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     */
    private $id;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $enabled;

    /**
     * @var string
     * @ORM\Column(type="string")
     * @Assert\Choice(callback={"App\Enum\TypeEnum", "getAll"})
     */
    private $type;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $number;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="string")
     * @Assert\Choice(callback={"App\Enum\ClassEnum", "getAll"})
     */
    private $class;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Choice(callback={"App\Enum\RarityEnum", "getAll"})
     */
    private $rarity;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Choice(callback={"App\Enum\NationEnum", "getAll"})
     */
    private $nation;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\ManyToMany(targetEntity="Drop", inversedBy="ships")
     * @ORM\JoinTable(
     *    name="alc_ship_drop",
     *    joinColumns={@ORM\JoinColumn(name="ship_id", referencedColumnName="id")},
     *    inverseJoinColumns={@ORM\JoinColumn(name="drop_id", referencedColumnName="id")}
     * )
     */
    private $drops;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="ships")
     */
    private $users;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $wikiUrl;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    public function __construct()
    {
        $this->id = Uuid::uuid4();
        $this->enabled = false;
    }

    /**
     * @return \Ramsey\Uuid\UuidInterface
     */
    public function getId(): \Ramsey\Uuid\UuidInterface
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     * @return Ship
     */
    public function setEnabled(bool $enabled): Ship
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * @return string
     */
    public function getNumber(): string
    {
        return $this->number;
    }

    /**
     * @param string $number
     * @return Ship
     */
    public function setNumber(string $number): Ship
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Ship
     */
    public function setName(string $name): Ship
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Ship
     */
    public function setType(string $type): Ship
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getClass(): string
    {
        return $this->class;
    }

    /**
     * @param string $class
     * @return Ship
     */
    public function setClass(string $class): Ship
    {
        $this->class = $class;

        return $this;
    }

    /**
     * @return string
     */
    public function getRarity(): ?string
    {
        return $this->rarity;
    }

    /**
     * @param string $rarity
     * @return Ship
     */
    public function setRarity(?string $rarity): Ship
    {
        $this->rarity = $rarity;

        return $this;
    }

    /**
     * @return string
     */
    public function getNation(): string
    {
        return $this->nation;
    }

    /**
     * @param string $nation
     * @return Ship
     */
    public function setNation(?string $nation): Ship
    {
        $this->nation = $nation;

        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDrops(): \Doctrine\Common\Collections\Collection
    {
        return $this->drops;
    }

    /**
     * Add ship.
     *
     * @param Drop $drop
     * @return Ship
     */
    public function addDrop(Drop $drop): Ship
    {
        if (!$this->drops->contains($drop)) {
            $this->drops[] = $drop;
        }

        return $this;
    }

    /**
     * @param Drop $drop
     * @return Ship
     */
    public function removeDrop(Drop $drop): Ship
    {
        $this->drops->removeElement($drop);

        return $this;
    }

    /**
     * @return string
     */
    public function getWikiUrl(): ?string
    {
        return $this->wikiUrl;
    }

    /**
     * @param string $wikiUrl
     * @return Ship
     */
    public function setWikiUrl(string $wikiUrl): Ship
    {
        $this->wikiUrl = $wikiUrl;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return Ship
     */
    public function setCreatedAt(\DateTime $createdAt): Ship
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return Ship
     */
    public function setUpdatedAt(\DateTime $updatedAt): Ship
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

}
