<?php

namespace App\Repository;

use App\Entity\Ship;
use App\Entity\User;
use App\Enum\TypeEnum;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class ShipRepository
 *
 * @package App\Repository
 */
class ShipRepository extends EntityRepository
{
    public function __construct(EntityManager $em, Mapping\ClassMetadata $class)
    {
        parent::__construct($em, $class);
    }

    /**
     * @return array
     */
    public function findAllWithUserPossession(User $user) : array
    {
        $sql =
            "SELECT s.id, s.name, s.type, s.number, s.rarity, s.wiki_url, CAST(CASE WHEN us.user_id IS NULL THEN 0 ELSE 1 END AS BIT) AS owned " .
            "FROM alc_ship s " .
            "LEFT JOIN alc_user_ship us ON s.id = us.ship_id AND us.user_id = :userId " .
            "WHERE s.enabled = :enabled " .
            "ORDER BY s.number"
        ;

        $rsm = new ResultSetMapping();
        $rsm->addEntityResult(Ship::class, 's');
        $rsm->addFieldResult('s', 'id', 'id');
        $rsm->addFieldResult('s', 'name', 'name');
        $rsm->addFieldResult('s', 'type', 'type');
        $rsm->addFieldResult('s', 'number', 'number');
        $rsm->addFieldResult('s', 'rarity', 'rarity');
        $rsm->addFieldResult('s', 'wiki_url', 'wikiUrl');
        $rsm->addScalarResult('owned', 'owned', 'boolean');

        $query = $this->_em->createNativeQuery($sql, $rsm);
        $query->setParameter('userId', $user->getId());
        $query->setParameter('enabled', true);
        $results = $query->getResult();

        return array_map(function ($result) {
            $result[0]->owned = $result['owned'];
            return $result[0];
        }, $results);
    }

    /**
     * @return Ship
     */
    public function findOneWithUserPossession(User $user, $id) : Ship
    {
        $sql =
            "SELECT s.id, s.name, s.type, s.number, CAST(CASE WHEN us.user_id IS NULL THEN 0 ELSE 1 END AS BIT) AS owned " .
            "FROM alc_ship s " .
            "LEFT JOIN alc_user_ship us ON s.id = us.ship_id AND us.user_id = :userId " .
            "WHERE s.id = :shipId"
        ;

        $rsm = new ResultSetMapping();
        $rsm->addEntityResult(Ship::class, 's');
        $rsm->addFieldResult('s', 'id', 'id');
        $rsm->addFieldResult('s', 'name', 'name');
        $rsm->addFieldResult('s', 'type', 'type');
        $rsm->addFieldResult('s', 'number', 'number');
        $rsm->addScalarResult('owned', 'owned', 'boolean');

        $query = $this->_em->createNativeQuery($sql, $rsm);
        $query->setParameter('userId', $user->getId());
        $query->setParameter('shipId', $id);
        $result = $query->getOneOrNullResult();
        $result[0]->owned = $result['owned'];

        return $result[0];
    }

    public function findAllWithDrops()
    {
        return $this->createQueryBuilder('s')
            ->leftJoin('s.drops', 'd')
            ->addSelect('d')
            ->orderBy('s.number', 'ASC')
            ->addOrderBy('d.description', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findOwned(User $user)
    {
        return $this->createQueryBuilder('s')
            ->addSelect('d')
            ->leftJoin('s.drops', 'd')
            ->leftJoin('s.users', 'u', 'WITH', 'u = :user')
            ->where('u IS NULL')
            ->andWhere('s.type = :type')
            ->andWhere('s.enabled = :enabled')
            ->orderBy('s.name', 'ASC')
            ->setParameter('user', $user)
            ->setParameter('type', TypeEnum::TYPE_STANDARD)
            ->setParameter('enabled', true)
            ->getQuery()
            ->getResult()
        ;
    }
}
