<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Class IterableExtension
 *
 * @package App\Twig
 */
class IterableExtension extends AbstractExtension
{
    /**
     * @return array
     */
    public function getFilters() : array
    {
        return [
            new TwigFilter('naturalSorting', [$this, 'naturalSorting']),
        ];
    }

    /**
     * @param $collection
     * @return array
     */
    public function naturalSorting($collection) : array
    {
        $array = !\is_array($collection) ? $collection->toArray() : $collection;

        $collator = collator_create('root');
        $collator->setAttribute(\Collator::NUMERIC_COLLATION, \Collator::ON);
        $collator->asort($array);

        return $array;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return 'iterable_extension';
    }
}
