<?php

namespace App\Controller\Admin;

use App\Entity\Drop;
use App\Entity\Ship;
use App\Form\Type\Entity\ShipType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ShipController
 *
 * @package App\Controller
 *
 * @Route("/admin/ships")
 * @IsGranted("ROLE_ADMIN")
 */
class ShipController extends AbstractController
{
    /**
     * @Route("", name="app_admin_ship_index", methods={"GET"})
     *
     * @return Response
     */
    public function index() : Response
    {
        $em = $this->getDoctrine()->getManager();

        return $this->render('App/Admin/Ship/index.html.twig', [
            'ships' => $em->getRepository(Ship::class)->findAllWithDrops()
        ]);
    }

    /**
     * @Route("/{id}", name="app_admin_ship_show", methods={"GET"})
     *
     * @return Response
     */
    public function show(Ship $ship) : Response
    {
        return $this->render('App/Admin/Ship/show.html.twig', [
            'ship' => $ship,
            'drops' => $this->getDoctrine()->getRepository(Drop::class)->findAll()
        ]);
    }

    /**
     * @Route("/{id}/enable", name="app_admin_ship_enable", methods={"GET"})
     *
     * @return Response
     */
    public function enable(Ship $ship) : Response
    {
        $ship->setEnabled(!$ship->isEnabled());
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse();
    }

    /**
     * @Route("/{id}/add-drop/{dropDesc}", name="app_admin_ship_add_drop", methods={"GET"})
     *
     * @return Response
     */
    public function addDrop(Ship $ship, $dropDesc) : Response
    {
        $drop = $this->getDoctrine()->getRepository(Drop::class)->findOneBy(['description' => $dropDesc]);
        $ship->addDrop($drop);
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse([
            'id' => $drop->getId(),
            'description' => $drop->getDescription(),
            'shipId' => $ship->getId()
        ]);
    }
    /**
     * @Route("/{id}/remove-drop/{dropId}", name="app_admin_remove_drop", methods={"GET"})
     *
     * @return Response
     */
    public function removeDrop(Ship $ship, $dropId) : Response
    {
        $drop = $this->getDoctrine()->getRepository(Drop::class)->find($dropId);
        $ship->removeDrop($drop);
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse([
            'id' => $drop->getId(),
            'description' => $drop->getDescription(),
            'shipId' => $ship->getId()
        ]);
    }

}
