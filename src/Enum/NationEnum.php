<?php

namespace App\Enum;

/**
 * Class NationEnum
 *
 * @package App\Enum
 */
class NationEnum extends Enum
{
    const NATION_UNIVERSAL = 'universal';
    const NATION_SAKURA_EMPIRE = 'sakura-empire';
    const NATION_EAGLE_UNION = 'eagle-union';
    const NATION_ROYAL_NAVY = 'royal-navy';
    const NATION_IRONBLOOD = 'ironblood';
    const NATION_EASTERN_RADIANCE = 'eastern-radiance';
    const NATION_NORTH_UNION = 'north-union';
    const NATION_IRIS_LIBRE = 'iris-libre';
    const NATION_VICHYA_DOMINION = 'vichya-dominion';
}
