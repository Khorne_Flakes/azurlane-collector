<?php

namespace App\Controller;

use App\Entity\Drop;
use App\Entity\Ship;
use App\Entity\User;
use App\Form\Type\Security\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Class DropController
 *
 * @package App\Controller
 *
 * @Route("")
 */
class DropController extends AbstractController
{
    /**
     * @Route("/drops/autocomplete/{search}/ship/{shipId}", name="app_drop_autocomplete", methods={"GET"})
     *
     * @param string $search
     * @return JsonResponse
     */
    public function autocomplete(string $search, $shipId) : JsonResponse
    {
        $ship = $this->getDoctrine()->getRepository(Ship::class)->find($shipId);
        $drops = $this->getDoctrine()->getRepository(Drop::class)->findLike($search, $ship);
        $data = [];
        foreach ($drops as $drop) {
            $data[] = [
                'id' => $drop->getId(),
                'description' => $drop->getDescription()
            ];
        }

        return new JsonResponse($data);
    }
}
