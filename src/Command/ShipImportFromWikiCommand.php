<?php

namespace App\Command;

use App\Entity\Ship;
use App\Enum\TypeEnum;
use Doctrine\DBAL\Types\Type;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ShipImportFromWikiCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('alc:ship:import')
            ->setDescription('Import ships from wiki')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');

        //Get the whole HTML page
        $dom = new \DOMDocument('1.0');
        $dom->loadHTMLFile('https://azurlane.koumakan.jp/List_of_Ships_by_Image');

        $finder = new \DomXPath($dom);
        $typeNodes = $finder->query("//*[contains(@class, 'mw-headline')]");

        foreach ($typeNodes as $typeNode) {
            $type = $this->guessType($typeNode->childNodes[0]->wholeText);
            if ($type === TypeEnum::TYPE_COLLAB) {
                $nextSibling = $typeNode->parentNode->nextSibling->nextSibling;
                while ($nextSibling->nodeName === 'p') {
                    $shipNodes = $nextSibling->nextSibling->nextSibling->childNodes;
                    foreach ($shipNodes as $shipNode) {
                        if ($shipNode->nodeName == 'div') {
                            $this->handleShip($shipNode, $type, $em, $output);
                        }
                    }
                    $nextSibling = $nextSibling->nextSibling->nextSibling->nextSibling->nextSibling;
                }
            } else {
                $shipNodes = $typeNode->parentNode->nextSibling->nextSibling->childNodes;
                foreach ($shipNodes as $shipNode) {
                    if ($shipNode->nodeName == 'div') {
                        $this->handleShip($shipNode, $type, $em, $output);
                    }
                }
            }
        }
    }

    private function handleShip($shipNode, $type, $em, $output)
    {
        //Extract all the ship elements
        $number = $shipNode->firstChild->childNodes[0]->childNodes[1]->childNodes[0]->childNodes[0]->wholeText;

        if ($type === TypeEnum::TYPE_RETROFIT) {
            $number = '3'.$number;
        }

        $ship = $em->getRepository(Ship::class)->findOneBy([
            'number' => $number,
            'type' => $type
        ]);

        if (!$ship instanceof Ship) {
            $nationNode = $shipNode->firstChild->childNodes[2]->childNodes[0]->childNodes[0];
            $rarityNode = $shipNode->firstChild->childNodes[2]->childNodes[1]->childNodes[0];

            $ship = (new Ship())
                ->setNumber($number)
                ->setClass($this->slugify($shipNode->firstChild->childNodes[0]->childNodes[0]->childNodes[0]->childNodes[1]->childNodes[0]->wholeText))
                ->setNation($nationNode ? $this->slugify($nationNode->childNodes[1]->childNodes[0]->wholeText) : null)
                ->setRarity($rarityNode ? $this->slugify(substr($rarityNode->attributes[0]->value, 0, -4)) : null)
                ->setName($shipNode->firstChild->childNodes[3]->childNodes[0]->childNodes[0]->childNodes[0]->wholeText)
                ->setType($type)
                ->setWikiUrl('https://azurlane.koumakan.jp'. $shipNode->firstChild->childNodes[3]->childNodes[0]->childNodes[0]->attributes[0]->value)
            ;

            $directory = __DIR__ . '/../../assets/img/ships/'. $ship->getType();
            !is_dir($directory) && !mkdir($directory) && !is_dir($directory);

            $imgUrlNode = $shipNode->firstChild->childNodes[1]->childNodes[0]->childNodes[0]->attributes[1];
            if ($imgUrlNode) {
                copy('https://azurlane.koumakan.jp' . $imgUrlNode->value, $directory . '/' . $ship->getNumber() . '.png');
            }

            $em->persist($ship);
            $em->flush();

            $output->writeln('Ship ' . $ship->getNumber() . ' ' . $ship->getName() . ' (' . $ship->getType() . ') added.');
        }
    }

    private function slugify(string $string)
    {
        return str_replace(' ', '-', strtolower($string));
    }

    private function guessType(string $string)
    {
        $type = null;

        switch ($string) {
            case 'Standard Ships':
                $type = TypeEnum::TYPE_STANDARD;
                break;
            case 'Collab Ships':
                $type = TypeEnum::TYPE_COLLAB;
                break;
            case 'PR Ships':
                $type = TypeEnum::TYPE_PR;
                break;
            case 'Retrofitted Ships':
                $type = TypeEnum::TYPE_RETROFIT;
                break;
            default:
                break;
        }

        return $type;
    }
}
