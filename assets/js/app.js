import './lazy-loading'

import 'jquery'
import 'materialize-css'

document.addEventListener('DOMContentLoaded', function() {
    M.FormSelect.init(document.querySelectorAll('select'));
});

$(document).ready(function() {
    //Init selects
    $('.collapse-nav').click(function (e) {
        e.preventDefault();
        var collapse = !$('body').hasClass('collapsed');
        if (collapse) {
            $('body').addClass('collapsed');
            Cookies.set('sidebar_collapsed', 1);
        } else {
            $('body').removeClass('collapsed');
            Cookies.remove('sidebar_collapsed');
        }
    });

    $('.burger-menu, .close-nav').click(function (e) {
        e.preventDefault();
        var open = !$('body').hasClass('mobile-opened');
        if (open) {
            $('body').addClass('mobile-opened');
        } else {
            $('body').removeClass('mobile-opened');
        }
    });

    $('.mobile-overlay').click(function (e) {
        e.preventDefault();
        $('body').removeClass('mobile-opened');
    });

    $('.enabled-switch').click(function (e) {
        $.get( "/admin/ships/" + $(this).data('id') + "/enable", function( data ) {
        });
    });

    $('.owned-checkbox').click(function (e) {
        let $this = $(this);
        let $ship = $this.closest('.ship').first();
        $this.attr('disabled', true);

        $.get( "/ships/" + $(this).data('id') + "/own", function( data ) {
            if (data == true) {
                $ship.attr('data-owned', 1);
                $ship.find('.image-wrapper').removeClass('grayscaled');
            } else {
                $ship.attr('data-owned', 0);
                $ship.find('.image-wrapper').addClass('grayscaled');
            }
            $this.removeAttr('disabled');
        })
    });

    $('input[type=radio][name=owned]').change(function() {
        filterOnShipOwned(this);
    });

    $('input[type=radio][name=enabled]').change(function() {
        filterOnShipEnabled(this)
    });

    $('input[type=checkbox][name=type]').click(function() {
        filterOnShipType(this)
    });

    $('.name-filter').keyup( function() {
        filterOnShipName(this)
    });

    $('.ship-drops-table').on("click", '.drop .fa-close', function() {
        let $self = $(this);

        $.get('/admin/ships/' + $self.attr('data-ship-id') + '/remove-drop/' + $self.attr('data-drop-id'), function( result ) {
            $self.closest('.drop').remove();
            $('.ship-drop-select').append('<option value="' + result.id + '">' + result.desc + '</option>')
        });
    });

    //AUTOCOMPLETE
    var dropsAutocomplete = M.Autocomplete.init(document.querySelectorAll('.autocomplete'), {
        onAutocomplete: function (item) {
            onAutocomplete(item);
        }
    })[0];

    $('#dropsAutocomplete').keyup(function (e) {
        dropsAutocomplete.updateData({});
        if ($(this).val() != '') {
            $.get('/drops/autocomplete/' + $(this).val() + "/ship/" + $('#dropsAutocomplete').attr('data-ship-id'), function( result ) {
                var data = {};
                $.each(result, function( key, drop ) {
                    data[drop.description] = null;
                });

                dropsAutocomplete.updateData(data);
                dropsAutocomplete.open();
            }, "json" );
        }

        if(e.which == 13) {
            onAutocomplete($(this).val());
        }
    });

    function onAutocomplete(value)
    {
        if ( value != '' ) {
            $.get('/admin/ships/' + $('#dropsAutocomplete').attr('data-ship-id') + '/add-drop/' + value, function (result) {
                $('.ship-drops-table tbody').append('<tr class="list-element drop"><td>' + result.description +'</td><td><i class="fa fa-close fa-fw" data-ship-id="' + result.shipId + '" data-drop-id="' + result.id + '"></i><span></span></td></tr>')
            });
        }
    }

    function filterOnShipName (element) {
        var regex = new RegExp(element.val(), 'i');
        $('.ship').each(function() {
            if (element.attr('data-name').match(regex)) {
                element.removeClass('hidden');
            } else {
                element.addClass('hidden');
            }
        });
        var event = new Event('filter');
        window.dispatchEvent(event);
        updateShipTypesCounters();
    }

    function filterOnShipOwned (element) {
        if (element.value == '0') {
            $('.ship[data-owned="1"]').addClass('hidden');
            $('.ship[data-owned="0"]').removeClass('hidden');
        } else if (element.value == '1') {
            $('.ship[data-owned="1"]').removeClass('hidden');
            $('.ship[data-owned="0"]').addClass('hidden');
        } else {
            $('.ship').removeClass('hidden');
        }
        var event = new Event('filter');
        window.dispatchEvent(event);
        updateShipTypesCounters();
    }

    function filterOnShipEnabled (element) {
        if (element.value == '0') {
            $('.ship[data-enabled="1"]').addClass('hidden');
            $('.ship[data-enabled="0"]').removeClass('hidden');
        } else if (element.value == '1') {
            $('.ship[data-enabled="1"]').removeClass('hidden');
            $('.ship[data-enabled="0"]').addClass('hidden');
        } else {
            $('.ship').removeClass('hidden');
        }
        var event = new Event('filter');
        window.dispatchEvent(event);
        updateShipTypesCounters();
    }

    function filterOnShipType (element) {
        if (element.checked) {
            $('.type-block[data-type="' + element.value + '"]').removeClass('hidden');
        } else {
            $('.type-block[data-type="' + element.value + '"]').addClass('hidden');
        }

        var event = new Event('filter');
        window.dispatchEvent(event);
    }

    function updateShipTypesCounters()
    {
        $('.ship-type-counter').each(function () {
            $(this).html($(this).parent().next().find('.ship:not(.hidden)').length);
        });
    }
});
